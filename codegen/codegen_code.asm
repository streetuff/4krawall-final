	codegen_ptr = $02
	codegen_src = $04
	codegen_tmp = $06

set_codegen_start:
	;in x low
	;in y high
	stx codegen_ptr
	sty codegen_ptr+1
rts_addr:
	rts

end_code_with_rts:
	lda #<rts_addr
	ldx #>rts_addr
	ldy #$01

copy_code_fragment:
	;in a low
	;in x high
	;in y size
	sta codegen_src
	stx codegen_src+1
	sty codegen_add+1
	dey
-	lda (codegen_src),y
	sta (codegen_ptr),y
	dey
	bpl -
	lda codegen_ptr
	clc
codegen_add:
	adc #$00
	sta codegen_ptr
	bcc +
	inc codegen_ptr+1
+	rts

sub_code_offset:
	;in a value to sub
	sta codegen_tmp
	lda codegen_ptr
	sec
	sbc codegen_tmp
	sta codegen_ptr
	bcs +
	dec codegen_ptr+1
+	rts


