!ifdef MEMORY_TAB_ASM {
} else {

MEMORY_TAB_ASM = 1

colors = $e8

;for spritescreen part

sintab1 = $4000
sintab2 = $4100
generated_code = $ff00

;for noise
randomsprites = $3800 ; -$3fff

;for circles
circletab = $4200 ;2 pages

;for "sinbar" effect
sinbar_codegenadr = $6000

sinbar_sprites = $c800
sinbat_addtab = $c100

sinbar_d018_table = $c200
sinbar_sintab1 = $c400
sinbar_sintab2 = $c500
sinbar_sintab3 = $3c00

;for ecmplasma
sinetab = $fc00  ; plasma

;for hypnotize
sine1 = $fe00    ;hypnotize

circle_y_movetab = $fa00
stretch_tab = $f900
stretch_sin = $4400

kefrens_first_ands = $f800
kefrens_first_ors = $f700
kefrens_last_ands = $f600
kefrens_last_ors = $f500
kefrens_addresses = $f3ff
kefrens_sintab = $4500
kefrens_sintab2 = $4700

codegen_kefrens = $4900
}

