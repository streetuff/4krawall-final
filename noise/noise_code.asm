BASE_VAL = $07

!src "include/memorytab.asm"

disable_badline_irq_noise:
	pha
	txa
	pha
	tya
	pha
irq_counter_noise:
	ldx #$00
	lda irq_low_tab_noise,x
	sta irq_jump_noise+1
	lda irq_high_tab_noise,x
	sta irq_jump_noise+2
	lda newlinetab_noise,x
	sta $d012
	inc $d019
	inx
	cpx #MAX_IRQS_NOISE
	bne +
	ldx #$00
+	stx irq_counter_noise+1
irq_jump_noise:
	jsr $0
        jmp end_irq


!ifdef enable_spritescreen {
} else {
irq_line_ff:
	lda #$88
	sta $d011
	rts
}

irq_last_line:
	lda #$08
	sta $d011
	jsr disable_openborder_nmi
	inc wait+1
	lda #BASE_VAL
	jsr set_new_ypos
        !ifdef enable_sound {
	jmp sound_main
        } else {
	rts
	}	

irq_line_f8_2:
	jsr irq_line_f8
	lda #<(BASE_VAL+21*12)
	bne set_new_ypos

irq_line_ff_2:
	jsr irq_line_ff
	lda #<(BASE_VAL+21*13)
	bne set_new_ypos


set_new_ypos_enable_nmi:
	tay
        jsr enable_openborder_nmi
        tya
        bne set_new_ypos
        
set_new_ypos_alt:
	lda #BASE_VAL+21*11

set_new_ypos:
	sta $d001
	sta $d003
	sta $d005
	sta $d007
	sta $d009
	sta $d00b
	sta $d00d
	sta $d00f
	rts

precalc_noise:
	lda #$ff
	sta $d400+14
	sta $d401+14
	lda #$81
	sta $d404+14
	ldy #$09
	ldx #$00
-	lda $d41b
	eor $dc04
random_addr:
	sta randomsprites-$100,x
	inx
	bne -
	inc random_addr+2
	dey
	bne - 
	rts

noise:
	jsr waitvbl
	lda #$00
	sta $d020
	sta $d021
        sta $d017
        lda #$03
        sta $dd00
        lda #$15
        sta $d018        
	jsr reinit_openirq
	ldx #<disable_badline_irq_noise
	ldy #>disable_badline_irq_noise
	jsr set_irq
	jsr set_sprites_for_fullscreen
	lda #$0c
	sta $d025
	lda #$0f
	sta $d026
	ldx #$07
-
	txa
	ora #$e0
	sta $07f8,x
	dex
	bpl -
	jsr waitvbl
	lda #$ff
	sta $d01c
	jsr enable_openborder_nmi
	ldx #150
--	ldy #$07
spr_loop:
	lda randomsprites
	eor $d41b
	ora #$e0
	sta $07f8,y
        inc spr_loop + 1
	dey
	bpl spr_loop
wait:
	lda #$00
	beq --
        ror spr_loop + 1
	lda #$00
	sta wait+1
	dex
	bne --
	jmp set_standard_irq



