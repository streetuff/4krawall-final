irq_low_tab_noise:
	!byte <irq_line_f8_2
	!byte <irq_line_ff_2
	!byte <irq_last_line
        !byte <set_new_ypos_enable_nmi
	!for i, 9 {
	!byte <set_new_ypos
	}
	!byte <set_new_ypos_alt

irq_high_tab_noise:
	!byte >irq_line_f8_2
	!byte >irq_line_ff_2
	!byte >irq_last_line
        !byte >set_new_ypos_enable_nmi
	!for i, 9 {
	!byte >set_new_ypos
	}
	!byte >set_new_ypos_alt

newlinetab_noise:
	!byte $fe
	!byte $28+7-BASE_VAL
        !byte BASE_VAL-1
	!for i, 10 {
	!byte BASE_VAL+21*(i)
	}
	!byte $f0
MAX_IRQS_NOISE = *-newlinetab_noise
