endpic_vic:
	!byte 100,130-4
	!byte 102,132-4
	!byte 140,130-4
	!byte 142,132-4
	!byte 180,130-4
	!byte 182,132-4
	!byte 220,130-4
	!byte 222,132-4
	!byte $00 ; $d010
	!byte $1b ; $d011
	!byte $f9 ; $d012
	!byte $00 ; $d013
	!byte $00 ; $d014
	!byte $ff ; $d015
	!byte $00 ; $d016
	!byte $ff ; $d017
	!byte $15 ; $d018
	!byte $81 ; $d019
	!byte $81 ; $d01a
	!byte $00 ; $d01b
	!byte $00 ; $d01c
	!byte $ff ; $d01d
	!byte $00 ; $d01e
	!byte $00 ; $d01f
	!byte $06 ; $d020
	!byte $04 ; $d021
	!byte $00 ; $d022
	!byte $00 ; $d023
	!byte $00 ; $d024
	!byte $00 ; $d025
	!byte $00 ; $d026
	!for i, 4 {
	!byte $0d ; $d027+x
	!byte $05 ; $d028+x
	}
endpic_vic_len = * - endpic_vic


