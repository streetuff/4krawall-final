!src "include/memorytab.asm"

	D020_ADDR_OF_CODE = $0200

d020:
	!ifdef enable_spritescreen {
	} else {
	lda #>sintab2
	sta $fb
	ldy #$7f
	lda #09
	ldx #$fc
        jsr calcsintab
	}

	ldx #$00
	stx $d011
	lda #$e0
	sta $d017
	sta $d015
	lda #$80
	sta $d00b
	sta $d00d
	sta $d00f

-	lda d020_code_start,x
	sta D020_ADDR_OF_CODE,x
        inx
        cpx #d020_code_len
        bne -

        lda colors
        sta col0_ptr1
        sta col0_ptr2
        lda colors+1
        sta col1_ptr1
        sta col1_ptr2
        lda colors+2
        sta col2_ptr1
        sta col2_ptr2
        lda colors+3
        sta col3_ptr1
        ;sta col3_ptr2

        !ifdef enable_sound {
        } else {
	lda #150
	sta $02
        }
        jmp D020_ADDR_OF_CODE

d020_code_start:
	!pseudopc D020_ADDR_OF_CODE {
d020_loop
	jsr waitvbl
	sei
d020_value = *+1
	ldx #$30
	lda sintab2,x
	clc
	adc #$46
	sta $d00b
	txa
	sbx #$60
	lda sintab2,x
	clc
	adc #$90
	sta $d00d
	txa
	sbx #$60
	lda sintab2,x
	clc
	adc #$d0
	sta $d00f
	lax d020_value
d020_sbx_val = *+1
	sbx #$02
	stx d020_value
	lda #$16
-	cmp $d012
	bne -
	jsr waitstable

scroll_pix:
	lda #$00
        sta bpl_addr+1
bpl_addr:
	bpl *        
	!for i, 31 {
	cmp #$c9
        }
	bit $ea

        ldx #$01
	ldy #$00
c:
col0_ptr1 = *+1
        lda #$00 ;06
-
c0:	sta $d020
col1_ptr1 = *+1
        lda #$00 ;04
c1:        
        sta $d020
col2_ptr1 = *+1
        lda #$00 ;0e
c2:
        sta $d020
col3_ptr1 = *+1
        lda #$00 ;03
c3:
        sta $d020
        ;lda #$01
c4:
        stx $d020
c5:
        sta $d020
col2_ptr2 = *+1
        lda #$00 ; 0e
c6:
        sta $d020
col1_ptr2 = *+1
        lda #$00 ; 04
c7:
        sta $d020
        lda #$00 ; 0b
c8:     sta $d020
	lda #$00
c9:
        sta $d020
col0_ptr2 = *+1
	lda #$00 ; 06
	dey
        bne -
        lax scroll_pix+1
        sbx #$fd
        cpx #63
        bcc +
        ldx #$00
+       stx scroll_pix+1
	!ifdef enable_sound {
	jsr sound_main
	}
	!ifdef enable_sound {
        lda timer
	bne +
	}
	jmp d020_loop
+
	lda #$03
	sta d020_sbx_val-D020_ADDR_OF_CODE+d020_code_start	
	jmp set_standard_irq
	}
d020_code_len = * - d020_code_start

