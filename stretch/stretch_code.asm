!src "include/memorytab.asm"

!cpu 6510

x_base          = $27
y_base          = $3f

colbyte         = $fb
counter         = $fc
switchbyte      = $fd

normal_color    = $0d
bg_color        = $05

stretch_counter = $ff

; ________________________ INIT SINTAB VIA ULLIS SINCALC
stretch_init:
	lda #>stretch_sin
	sta $fb
	ldy #$3f
	lda #$30
	ldx #$34
        jsr calcsintab
	ldx #$00
-	lda stretch_sin,x
	sta stretch_sin+$80,x
	inx
	bpl -
	rts

stretch:
  ;sei
; ________________________ SETUP SPRITES, GHOSTBYTE ETC...

  !byte $ab, $00          ; = lxa #$00
  tay
  stx $d021               ; border black
  stx $d01d               ; no x expansion!!!
  stx switchbyte          ; reset switchbyte
- 
  lda krawallblocks,x
  sta $0400+$03f8,x       ; spritepointer setup
                          ; could save 3 bytes - probably the same as
                          ; jsr setup_spritesblocks_4krawall in spritescreen
                          
  lda x_table,x           ; x-coords. from table
  sta $d000,y             

  lda #y_base             ; y-coords. fixed
  sta $d001,y
.a1 =*+1  
  lda #normal_color
  sta $d027,x
  
  iny
  iny
  
  stx colbyte             ; workaround to get $07 into colbyte
                          ; wastes cycles but saves bytes
                          ; timing doesn't has to be exact here anyway
  inx
  
  cpx #$08
  bne -
    
  stx counter
  
  lda #$c0
  sta $d010               ; sprite x MSB
  
  lda #$ff
  sta $d01b               ; set priority
  sta $d015               ; show sprites

  !ifdef enable_sound {
  } else {
  lda #250
  sta stretch_counter
  }
; ________________________ MAINLOOP
  
stretch_loop:

  ;!ifdef enable_sound {
  ;jsr sound_main
  ;}

  lda #$f9                ; wait for line $f9
-
  cmp $d012
  bne -
  lda #$15
  sta $d018

  lda #$03                ; open border & turn off screen
  sta $d011
  
  lda #$36
- cmp $d012
  bne -

  lda #$0b                ; prepare $d011 for next frame
  sta $d011   
; ________________________ GHOSTBYTE STUFF
  lda switchbyte
  lsr
  lsr
  ror
  bcc .dir1

.dir2 
  clc
  ror $3fff               ; empty ghostbyte
  lda counter
  bne ++

  ldx #$07
-
  lda krawallblocks,x
  sta $0400+$03f8,x
  dex
  bpl -
.a2 =*+1
  lda #bg_color
  sta $d021

  lda #$1a                ; fiddle with this value
                          ; to change how long $3fff stays blank
  jmp +
  
.dir1 
  sec
  rol $3fff               ; fill ghostbyte
  lda counter
  bne ++

  ldx #$03
-
  lda trsiblocks,x
  sta $0400+$03f8,x
  dex
  bpl -
  
  stx $d021
  
  lda #$0d                ; fiddle with this value
                          ; to change how long $3fff stays filled
+
  inc switchbyte

  sta counter
++
  dec counter

; ________________________ CALCULATE SPRITE STRETCH TABLE
  
                          ; !!!! ACHTUNG !!!!
	ldy #$fa                ; dirty workaround 
	                        ; memory has to be initialised with $00 before!!!
	
	lda #$ff                ; init stretch table with $ff
-                         ; => all sprites stretched
	sta stretch_tab,y
	dey
	bne -
	
.sin_tab_pointer = *+1
  ldx #0
  
  lda stretch_sintable,x  ; load value from sintable
  tay                     ; (will be the offset in stretchtable)
  ldx #15                 ; write 16 times
  lda #0                  ; $00 (no sprites stretched)
-
  sta stretch_tab,y       ; into stretch table
  iny
  iny
  iny
  dex
  bpl -
  
  inc .sin_tab_pointer

  lda #y_base             ; use sprite y-pos
-                         ; for rastercompare
  cmp $d012
  bne -

	ldx #$04                ; waste some cycles
-
	dex
	bne -

; ________________________ SPRITE STRETCH LOOP
  
  ldx #$00
-  
  lda stretch_tab,x       ; 04 (04)
  sta $d017               ; 04 (08)
  
  jsr fake                ; 12 (20)
  bit $ea                 ; 03 (23)
  bit $ea                 ; 03 (26)
  bit $ea                 ; 03 (29)
  nop                     ; 02 (31)
  
  
  lda #$00                ; 02 (33)
  sta $d017               ; 04 (37)
  
  inx                     ; 02 (39)
  cpx #160                ; 02 (41)
  bne -                   ; 03 (44)

; ________________________ SPRITE COLOR & PRIORITY STUFF
  
  dec counter_2
  bne skip_colorcycle
  
  lda #$09
  sta counter_2

  lda colbyte
  sta mod1

  lda $d02e
  sta colbyte
  ldx #$06
-
  lda $d027,x
  sta $d028,x
  dex
  bpl -
mod1 =*+1
  lda #$00
  sta $d027
  
  lda $d01b
  cmp #$ff
  bne +
  clc
  !byte $24               ; skip sec
+
  sec
  rol $d01b               ; sync priorty with yellow letter
  
skip_colorcycle:

  !ifdef enable_sound {
  lda timer
  bne stretch_exit
  } else {
  dec stretch_counter
  beq stretch_exit
  }
  jmp stretch_loop
  
stretch_exit:
  inc .a1
  inc .a2
  jmp set_standard_irq
fake:
  rts

; ___________________________________________________________________ NOTES
a0_NOTES:
;           COMPARE AND BRANCHES
;           ====================
;                     +----------------------+----------------------+
;                     | UNSIGNED             | SIGNED               |
;           +---------+----------------------+----------------------+
;           | A  = B  |   lda  A             |   lda  A             |
;           |         |   cmp  B             |   cmp  B             |
;           |         |   beq .equal_to      |   beq .equal_to      |
;           +---------+----------------------+----------------------+
;           | A <> B  |   lda  A             |   lda  A             |
;           |[A != B] |   cmp  B             |   cmp  B             |
;           |         |   bne .not_equal_to  |   bne .not_equal_to  |
;           +---------+----------------------+----------------------+
;           | A  < B  |   lda  A             |   lda  A             |
;           |         |   cmp  B             |   cmp  B             |
;           |         |   bcc .less_than     |   bmi .less_than     |
;           +---------+----------------------+----------------------+
;           | A  > B  |   lda  A             |   lda  A             |
;           |         |   cmp  B             |   cmp  B             |
;           |         |   beq .skip          |   beq .skip          |
;           |         |   bcs .greater_than  |   bpl .greater_than  |
;           |         |.skip                 |.skip                 |
;           +---------+----------------------+----------------------+
;           | A <= B  |   lda  A             |   lda  A             |
;           |         |   cmp  B             |   cmp  B             |
;           |         |   bcc .ls_than_or_eq |   bmi .ls_than_or_eq |
;           |         |   beq .ls_than_or_eq |   beq .ls_than_or_eq |
;           +---------+----------------------+----------------------+
;           | A >= B  |   lda  A             |   lda  A             |
;           |         |   cmp  B             |   cmp  B             |
;           |         |   bcs .gr_than_or_eq |   bpl .gr_than_or_eq |
;           +---------+----------------------+----------------------+
