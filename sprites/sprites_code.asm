
setup_spritesblocks_4krawall:
	lda colors+1
	sta $d020
	sta $d021
	ldx #$08
-	lda krawallblocks-1,x
	sta $07f8-1,x
	lda colors+2
	sta $d027-1,x
	dex
	bne -
	lda #$01
-	sta d010ortab,x
	eor #$ff
	sta d010andtab,x
	eor #$ff
	inx
	sta d010ortab,x
	eor #$ff
	sta d010andtab,x
	eor #$ff
	inx
	asl
	bcc -
	rts

set_sprites_for_fullscreen:
	ldx #$0e
	ldy #$07
-	lda d000tab,y
	sta $d000,x
	lda #$06
	sta $d001,x
	lda colors+2
	sta $d027,y
	dey
	dex
	dex
	bpl -
        lda colors+1
        sta $d020
        sta $d021
	lda #$c1
	sta $d010
	lda #$ff
	sta $d015
	sta $d01d
	rts




