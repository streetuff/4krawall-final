!src "include/memorytab.asm"

!cpu 6510
;!to "halftone-ecm-plasma-codercolors4.prg",cbm
;--------------------------------------------------
sinetab1 =$b700
sinetab2 =$ba00
sinetab3 =$bb00
colors2	=$c000

xplasma	=$40
yplasma	=$68

xcnt		=$e6
ycnt		=$e7

xcnt01	=$e8
xcnt02	=$e9

ycnt01	=$ea
ycnt02	=$eb
;--------------------------------------------------
col1 = $06	;5
col2 = $04	;e
col3 = $0a	;a
;--------------------------------------------------


ecmplasma:
			lda colors
			sta colors1
			lda colors+1
			sta colors1+1
			sta colors1+3
			lda colors+2
			sta colors1+2
		
			ldx #$00
loop1x
		 	lda sinetab+191,x
		 	sta sinetab1,x
			lsr
			sta sinetab2,x
			lsr
			sta sinetab3,x
			txa
			lsr
			lsr
			lsr
			lsr
			lsr
			lsr
			tay
			lda colors1,y
			sta colors2,x

			inx
			bne loop1x
			lda #%01011011
			sta $d011
			
			
;			lda #$00
;			sta $d020
			
			
;			lda #$35
;			sta $01

;			lda #$01
;			sta $d01a
;			lda #<raster
;			sta $fffe
;			lda #>raster
;			sta $ffff
;			lda #$ff
;			sta $d012
;			lda $d011
;			and #$7f
;			sta $d011
;			lda #$7f
;			sta $dc0d
;			lda $dc0d
			
;			lda #$00
;			jsr $0800

;			cli
                        !ifdef enable_sound {
			} else {
			ldx #150
                        }

			lda #$c8
			sta $d016
			
			lda colors1+1
			sta $d021
			sta $d023
			lda colors1
			sta $d022
			sta $d020

			lda colors1+2
			sta $d024

main0
                        !ifdef enable_sound {
                        } else {
			txa
			pha
                        }
                        jsr waitstable
			jsr drawplasma
			lda #$01
			sta $dd00
			lda #%11111110
			sta $d018
                        !ifdef enable_sound {
                        lda timer
                        beq main0
                        } else {
			pla
			tax
			dex
			bne main0
                        }
                        !ifdef enable_sound {
			jsr waitvbl
			lda #$00
			sta $d020
			sta $d021
			inc xadd1
			inc xadd1
			inc yadd1
			inc yadd1

			jmp set_standard_irq_without_colors
                        } else {
                        rts
                        }
			;jmp main0
;--------------------------------------------------
raster
;			sta raster_old_a+1
;			stx raster_old_x+1
;			sty raster_old_y+1
main
;			dec $d020
;			jsr $0803
;			inc $d020

;			inc $d019
;raster_old_a:	lda #$00
;raster_old_x:	ldx #$00
;raster_old_y:	ldy #$00
;			rti



;--------------------------------------------------
drawplasma
		
		lda xcnt01
		sta xcnt1
		lda xcnt02
		sta xcnt2

;--------------------------------------------------
 			
		ldx #$00
		clc
loop4	
xcnt1 =*+1
		lda sinetab1

xcnt2 =*+1
		adc sinetab2
		sta xplasma,x
		
		lda xcnt1
		clc
		adc xadd1 
		sta xcnt1
		
		lda xcnt2
		clc
		adc xadd2 
		sta xcnt2
		
		inx
		cpx #40
		bne loop4
		
;--------------------------------------------------

		lda ycnt01
		sta ycnt1
		lda ycnt02
		sta ycnt2

;--------------------------------------------------
	
		ldx #$00
		clc
loop5
ycnt1 =*+1
		lda sinetab1
ycnt2 =*+1
		adc sinetab3
		sta yplasma,x

		lda ycnt1
		clc
		adc yadd1 
		sta ycnt1
		
		lda ycnt2
		clc
		adc yadd2 
		sta ycnt2
		
		inx
		cpx #25
		bne loop5

;--------------------------------------------------

		lda xcnt01
		clc
		adc xadd01 
		sta xcnt01

		lda xcnt02
		clc
		adc xadd02 
		sta xcnt02

		lda ycnt01
		clc
		adc yadd01 
		sta ycnt01

		lda ycnt02
		clc
		adc yadd02 
		sta ycnt02
		
;--------------------------------------------------
		
		ldx ycnt
		lda sinetab3,x
		lsr
		lsr
		sta yadd02
		
		ldx xcnt
		lda sinetab3,x
		lsr
		lsr
		sta xadd02
		
;--------------------------------------------------
		
;		lda ycnt
;		clc
;		adc yadd
;		sta ycnt
		
;		lda xcnt
;		clc
;		adc xadd
;		sta xcnt
	
;--------------------------------------------------
		lda #$3a
		cmp $d012
		bne *-3

		jmp $8000
;--------------------------------------------------

ecmplasma_init:
			lda #>sinetab
			sta $fb
			ldy #$ff
			lda #$7f
			ldx #$7f
			jsr calcsintab

			!ifdef enable_openborder {
			} else {
			sei
			lda #$35
			sta $01
			}
			lda #$00
			sta writebyte+1
			lda #$b8
			sta writebyte+2
			jsr genfont
			lda #$00
			sta writebyte+1
			lda #$80
			sta writebyte+2


			;jmp codegen

codegen
		ldy #$00
cgloop1
		ldx #$00
cgloop2
		lda snipet,x
		jsr writebyte
		inx
		cpx #14
		bne cgloop2
		inc mx2+1
		inc mx3+1
		bne nomx3
		inc mx3+2
nomx3

		inc mx4+1
		bne nomx4
		inc mx4+2
nomx4

		iny
		cpy #40
		bne cgloop1
                ;disable and enable to hide "dots"
                lda #$58
		jsr writebyte
		lda #$78
                jsr writebyte                
		lda #xplasma
		sta mx2+1
		inc mx1+1
		lda mx1+1
		cmp #yplasma+25
		beq lastcall
		jmp codegen
lastcall
		lda #$60
		jsr writebyte
		rts
;--------------------------------------------------
;writebyte
;		sta $2000
;		inc writebyte+1
;		bne nowrite
;		inc writebyte+2
;nowrite
;		rts
;--------------------------------------------------
snipet
mx1		lda yplasma		;2 inc every 40
mx2		adc xplasma 	;2 inc every value and reset after 40
		tay						;1
		ldx colors2,y	;3
mx4		stx $d800			;3 inc +1
mx3		sta $bc00			;3 inc +1
;--------------------------------------------------
;!src "genfont.asm"
;--------------------------------------------------

