
pattern_pos
!byte $00
play_voice2
!byte $00
;--------------------------------------------------
val1 !byte $00
val2 !byte $00
val3 !byte $00
sndoff	!byte $00
filterset	!byte $00

cycletab
		!word acycle0,acycle1,acycle1,acycle3,acycle4,acycle5
;--------------------------------------------------
drumpos !byte $00
drumtrackpos	!byte $00
drumtemp	!byte 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
;--------------------------------------------------
drumpattwlo !byte 0,<dwave1,<dwave2,<dwave3,<dwave4
drumpattwhi !byte 0,>dwave1,>dwave2,>dwave3,>dwave4
drumpattnlo !byte 0,<dnote1,<dnote2,<dnote3,<dnote1
drumpattnhi !byte 0,>dnote1,>dnote2,>dnote3,>dnote1
drumad		!byte 0,$0e,$0e,$04,$0e
drumsr		!byte 0,$f8,$f8,$28,$f8
;--------------------------------------------------
drumpattlo	!byte <dp,<dp+16,<dp+16,<dp+16,<dp+16,<dp+32,<dp+32,<dp+32,<dp+32,<dp+48,<dp+48,<dp+48,<dp+48,<dp+64
drumpatthi	!byte >dp,>dp+16,>dp+16,>dp+16,>dp+16,>dp+32,>dp+32,>dp+32,>dp+32,>dp+48,>dp+48,>dp+48,>dp+48,>dp+64
adv2:				!byte $00,$00,$11,$12,$13,$14,$15,$16,$17,$18,$19,$1a,$1b,$10,$ff
dp	; drumparts
					!byte 1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0
					!byte 1,0,0,0,2,0,0,0,1,0,0,0,2,0,0,0
					!byte 1,0,3,0,2,0,3,0,1,0,3,0,2,0,3,0
					!byte 1,0,3,0,2,0,3,0,1,0,3,0,2,0,3,3
					!byte 4,0,4,0,4,0,4,0,4,0,4,0,4,0,4,0
;--------------------------------------------------
dwave1			!byte $11,$11,$11,$11,$11,$11,$11,$11,$11,$11,$10,$10,$10,$10,$10,$10,$10,$ff ;waveform
dnote1			!byte $20,$28,$25,$1f,$1d,$19,$18,$1a,$16,$17,$14,$15,$12,$13,$10,$11,$0c,$ff ;notes

dwave2			!byte $11,$11,$81,$11,$11,$11,$11,$11,$11,$11,$10,$10,$10,$10,$10,$10,$10,$ff ;waveform
dnote2			!byte $20,$28,$38,$1f,$1d,$19,$18,$1a,$16,$17,$14,$15,$12,$13,$10,$11,$0c,$ff ;notes

dwave3			!byte $11,$80,$80,$10,$10,$10,$10,$10,$10,$ff ;waveform
dnote3			!byte $16,$54,$50,$15,$12,$13,$10,$11,$0c,$ff ;notes

dwave4			!byte $11,$11,$11,$11,$11,$11,$11,$10,$10,$10,$10,$10,$10,$10,$10,$10,$10,$ff ;waveform
;--------------------------------------------------
hfrq
!byte $01,$01,$01,$01,$01,$01,$01,$01
!byte $01,$01,$01,$02
ftablehi
!byte $02,$02,$02,$02
!byte $02,$02,$03,$03,$03,$03,$03,$04
!byte $04,$04,$04,$05,$05,$05,$06,$06
!byte $06,$07,$07,$08,$08,$09,$09,$0a
!byte $0a,$0b,$0c,$0d,$0d,$0e,$0f,$10
!byte $11,$12,$13,$14,$15,$17,$18,$1a
!byte $1b,$1d,$1f,$20,$22,$24,$27,$29
!byte $2b,$2e,$31,$34,$37,$3a,$3e,$41
!byte $45,$49,$4e,$52,$57,$5c,$62,$68
!byte $6e,$75,$7c,$83,$8b,$93,$9c,$a5
!byte $af,$b9,$c4,$d0,$dd,$ea,$f8,$fd

lfrq
!byte $16,$27,$38,$4b,$5f,$73,$8a,$a1
!byte $ba,$d4,$f0,$0e
ftablelo
!byte $2d,$4e,$71,$96
!byte $bd,$e7,$13,$42,$74,$a9,$e0,$1b
!byte $5a,$9b,$e2,$2c,$7b,$ce,$27,$85
!byte $e8,$51,$c1,$37,$b4,$37,$c4,$57
!byte $f5,$9c,$4e,$09,$d0,$a3,$82,$6e
!byte $68,$6e,$88,$af,$eb,$39,$9c,$13
!byte $a1,$46,$04,$dc,$d0,$dc,$10,$5e
!byte $d6,$72,$38,$26,$42,$8c,$08,$b8
!byte $a0,$b8,$20,$bc,$ac,$e4,$70,$4c
!byte $84,$18,$10,$70,$40,$70,$40,$78
!byte $58,$c8,$e0,$98,$08,$30,$20,$2e
;--------------------------------------------------
pattern01
!byte note_g, _slide+_note
!byte note_g, _slide+_note
!byte note_g, _note
!byte note_g, _note
!byte note_g, _accent
!byte note_g, _octdown+_note
!byte note_g, _note
!byte note_g, _accent
!byte note_ais, _note
!byte note_g, _accent
!byte note_ais, _octdown+_slide+_note
!byte note_ais, _octdown+_note
!byte note_ais, _octdown+_note
!byte note_ais, _note
!byte note_g, _accent
!byte note_ais, _octdown+_note

pattern02
!byte note_g, _slide+_note
!byte note_g, _slide+_octup+_note
!byte note_g, _octup+_note
!byte note_g, _octup+_slide+_note
!byte note_g, _octup+_accent
!byte note_g, _note
!byte note_g, _octup+_note
!byte note_g, _octup+_accent
!byte note_ais, _octup+_note
!byte note_g, _octup+_accent
!byte note_g, _note
!byte note_ais, _octup+_note
!byte note_g, _note
!byte note_ais, _octup+_note
!byte note_g, _octup+_note
!byte note_ais, _note
;--------------------------------------------------
