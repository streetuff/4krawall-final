;!cpu 6502
;!to "confusion303_1.prg",cbm

;--------------------------------------------------
sid		=$d400

f1lo		=sid
f1hi		=sid+1
f2lo		=sid+7
f2hi		=sid+8
f3lo		=sid+14
f3hi		=sid+15

wf1		=sid+4
wf2		=sid+11
wf3		=sid+18

ad1		=sid+5
sr1		=sid+6
ad2		=sid+12
sr2		=sid+13
ad3		=sid+19
sr3		=sid+20

volume	=sid+24

;speed = 5

plenght	= $f0
ppos		= $f1
cycle 	= $f2
note		= $f3
val			= $f4
params1	= $f5
params2	= $f6
wf1tmp	= $f7
dpos		= $f8
;timer		= $f9

note_c		=$00
note_cis	=$01
note_d		=$02
note_dis	=$03
note_e		=$04
note_f		=$05
note_fis	=$06
note_g		=$07
note_gis	=$08
note_a		=$09
note_ais	=$0a
note_h		=$0b
note_c1		=$0c

_slide		=$01
_accent		=$02
_octup		=$04
_octdown	=$08
_note			=$10
;--------------------------------------------------
;*= $0801
;!byte $0c,$08,$0a,$00,$9e,$32,$30,$36,$34,$00,$00,$00,$00

;	*=$0810
;				jmp init
;				jmp play
;				sei
;				jsr init
;
;sound_main:
;				lda #$fa
;				cmp $d012
;				bne *-3
;
;				dec $d020
;				jsr play
;
;				inc $d020
;				
;				ldx #$80
;				dex
;				bne *-1
;				jmp main
				
sound_init
		lda #%00111111	;full volume. low-pass,band-pass filter = on
		sta volume
		
		lda #$00
                sta timer
;		lda #%00000000	;cutoff freq
		sta sid+21
;		lda #%00000000
		sta sid+22

		sta sid+2 ;pulsewidth voice1
;		lda #$00
		sta cycle
		sta ppos
		sta dpos

		lda #$10	; filter
		sta val
		sta sid+22
		sta plenght
		
		lda #$06	;08
		sta sid+3 ;pulsewidth voice1
		
		lda #%11110011	;filter voice 1+2
		sta sid+23
		
		lda #$1a	;adsr voice 1
		sta ad1
		lda #$6f
		sta sr1

		lda #$aa	;adsr voice 2
		sta ad2
		lda #$19	;12	-> $10 - $19
		sta sr2

		jmp setdrumtrack
		
		
sound_main:	
                !ifdef enable_spritescreen {
                } else {
                inc $d020
                }	
     lda sndoff
     beq no_sdnoff
     jmp endpart

;     rts
no_sdnoff
		lda cycle
		asl
		tax
		lda cycletab,x
		sta cycjmp+1
		lda cycletab+1,x
		sta cycjmp+2

cycjmp		
                !ifdef enable_spritescreen {
		jmp $4c4c
                } else {
                jsr $4c4c
                dec $d020
                rts
                }	
endpart
     lda #$00
     sta wf1
     sta wf2
     lda #$81
     sta f3lo
     sta f3hi
     sta wf3
     lda #$07
     sta $d418

     sei
     lda #$03
     sta $dd00
     ;lda #$37
     ;sta $01
     ;jsr $e544
     ldx #$00
     lda #$01
     sta $d020
     sta $d021
     jsr waitvbl     
     lda #$e5
     sta $d018
     lda #$1b
     sta $d011
     lda #$c8
     sta $d016
     
     lda #%00001111
     sta $d015
     sta $d017
     sta $d01d
     
     ldx #16
     stx $3bf8
     inx
     stx $3bf9
     inx
     stx $3bfa
     inx
     stx $3bfb
     
     clc
     lda #$50
     sta $d000
     adc #50
     sta $d002
     adc #50
     sta $d004
     adc #50
     sta $d006
     lda #$80
     sta $d001
     sta $d003
     sta $d005
     sta $d007
     
     lda #$00
     sta $d027
     sta $d028
     sta $d029
     sta $d02a
     sta $d010
     sta $d01c

endmain     
     ldx #$00
-     
     lda $0980,x	;T
     and $d41b
     sta $0400,x
     lda $0880,x	;R
     and $d41b
     sta $0440,x
     lda $09c0,x	;S
     and $d41b
     sta $0480,x
     lda $0a00,x	;I
     and $d41b
     sta $04c0,x
     inx
     cpx #$3f
     bne -
     
     jmp endmain
		
acycle0	
;		jmp drums
playvoice1
		lda ppos
		asl
		tax
		lda pattern01,x
		sta note
		lda pattern01+1,x
		sta params1
		tax

		and #%00001100
		cmp #%00001100
		beq normal_note1
		cmp #$00
		beq normal_note1
		txa
		and #%00000100
		cmp #%00000100
		beq note_up1
		jmp note_down1
		
note_up1	lda #24
		sta adder1+1
		jmp player1

note_down1
		lda #0
		sta adder1+1
		jmp player1
normal_note1
		lda #12
		sta adder1+1
player1
		lda params1
		and #%00010000
		cmp #%00010000
		beq note_on1
		jmp endcycle1
note_on1
		lda note
		clc
adder1		adc #$00
		tax
		lda ftablelo,x
		sta f1lo
		lda ftablehi,x
		sta f1hi

		lda #$41
		sta wf1tmp
		jmp playvoice2
endcycle1
		lda #$00
		sta wf1tmp


playvoice2
		lda ppos
		asl
		tax
		lda pattern02,x
		sta note
		lda pattern02+1,x
		sta params2
		tax

		and #%00001100
		cmp #%00001100
		beq normal_note2
		cmp #$00
		beq normal_note2
		txa
		and #%00000100
		cmp #%00000100
		beq note_up2
		jmp note_down2
		
note_up2	lda #24
		sta adder2+1
		jmp player2

note_down2
		lda #0
		sta adder2+1
		jmp player2
normal_note2
		lda #12
		sta adder2+1
player2
		lda params2
		and #%00010000
		cmp #%00010000
		beq note_on2
		jmp endcycle2
note_on2
		lda note
		clc
adder2	adc #$00
		tax
		lda ftablelo,x
		sta f2lo
		lda ftablehi,x
		sta f2hi
endcycle2		

drums
		ldx ppos
		lda drumtemp,x
		
		cmp #4
		bne no_divisor
		ldy #$01
		sty timer_divisor
;		lda #%11110111	;filter voice 1+2
;		sta sid+23		
		dec val
no_divisor
		tay
                !ifdef enable_openborder {
                } else {
		sta $d020
                }
		beq nodrums
		tax
		lda drumpattnlo,x
		sta setdrumn+1
		lda drumpattnhi,x
		sta setdrumn+2
		lda drumpattwlo,x
		sta setdrumw+1
		lda drumpattwhi,x
		sta setdrumw+2
		
		lda drumad,y
		sta ad3
;		sta $d020
		lda drumsr,y
		sta sr3
		lda #$00
		sta drumpos
		
		inc val3
		lda val3
		and #31
		bne nodrums
		inc drumtrackpos
		lda drumtrackpos
		and #15
		sta drumtrackpos
		jsr setdrumtrack

nodrums
		jsr playdrums

		lda play_voice2
		beq voice1

		lda #%00100001
		sta wf2
voice1
		ldx wf1tmp
		stx wf1

		inc cycle
		rts

acycle1
		jsr playdrums
		inc cycle
		rts

;acycle2
;		jsr playdrums
;		inc cycle
;		rts

acycle3	
		jsr acycle4
		lda params1
		and #%00000001
		cmp #%00000001
		beq slider1
		lda #%00000000
		sta wf1
slider1

		lda params2
		and #%00000001
		cmp #%00000001
		beq slider2
		lda #%00000000
		sta wf2
slider2	
;		inc cycle
		rts

acycle4
		ldx drumpos
		inx
		lda drumtemp,x
		beq noadhr2
		lda #$ff
		sta ad3
		lda #$00
		sta sr3
		jsr playdrums
noadhr2
		inc cycle
		rts
		
acycle5
		ldx drumpos
		inx
		lda drumtemp,x
		beq noadhr3
		lda #$09
		sta wf3
		jsr playdrums
noadhr3
		lda #$00
		sta cycle
                inc val2
                lda val2
timer_divisor = *+1
                and #$3f
                bne notimer
                inc timer
notimer                
		;inc val			; filter tmp
		inc ppos
		lda ppos
		cmp plenght
		bne no_p_reset
		lda #$00
		sta ppos
		jsr filter_triangle
		jsr filter_triangle
no_p_reset

		rts
filter_triangle
		inc val
triangle
		lda val
		sta sid+22
		cmp #$30
		beq downer
		cmp #$10
		beq upper		

		rts
;--------------------------------------------------
downer
		lda #$c6
		sta filter_triangle
		lda #$01
		sta play_voice2
		rts

upper
		lda #$e6
		sta filter_triangle
		rts
;--------------------------------------------------
setdrumtrack
		ldx drumtrackpos
		lda drumpattlo,x
		sta setdrum+1
		lda drumpatthi,x
		sta setdrum+2
                
		lda adv2,x
		cmp #$ff
		beq soundoff
		sta sr2

		ldx #$00
setdrum
		lda $0000,x
		sta drumtemp,x
		inx
		cpx #16
		bne setdrum
		rts
soundoff
		inc sndoff
		rts
;--------------------------------------------------
playdrums
		ldy drumpos
setdrumn
		lax $0000,y ;note
		lda lfrq,x
		sta f3lo
		lda hfrq,x
		sta f3hi

setdrumw
		lda $0000,y ;wave
		sta wf3
		
		inc drumpos

		rts

;playdrums
;		ldx drumpos
;setdrumn
;		lda $0000,x ;note
;		tay
;		lda lfrq,y
;		sta f3lo
;		lda hfrq,y
;		sta f3hi
;
;setdrumw
;		lda $0000,x ;wave
;		sta wf3
;		
;		inc drumpos
;
;		rts

;--------------------------------------------------
