
enable_openborder_nmi:
	ldx #$00
	lda #$82
	sta $dd0d,x
	rts

disable_openborder_nmi:
	lda #$7f
	sta $dd0d
	rts

disable_badline_irq:
	pha
	txa
	pha
	tya
	pha
disable_badline_irq_value:
	lda #$00
        sta $d011
        eor #$08
        sta disable_badline_irq_value+1
        inc $d019
        cmp #$08
        lda #$ff
        bcs +
        lda #$f8
+	sta $d012
	cmp #$ff
	bne +
	!ifdef enable_sound {
        cli
	jsr sound_main
	}
+	jmp end_irq

openborder_prepare:
	sei
        jsr init_stable_timer

	lda #$f8
        sta $d012
	lda #$1b
	sta $d011
        lda #<disable_badline_irq
        sta $fffe
        lda #>disable_badline_irq
        sta $ffff
        lda #$81
        sta $d01a
        lda #$7f
        sta $dc0d
        lda $dc0d
        lda #$35
        sta $01       
        ;cli

        jsr waitvbl
;	ldx #$00
;l       lda #$02
;	sta $2000,x
;	inx
;        bne l
;        inc l+4
;        lda l+4
;        cmp #$40
;        bne l                        
        ;lda oldcia
        ;beq +
        ;lda #$ea
        ;sta verz
+ 	ldx #code_max-1
-
	lda code_2b00,x
	sta $2b00,x
	lda code_2c00,x
	sta $2c00,x
	lda code_2d00,x
	sta $2d00,x
	lda code_2e00,x
	sta $2e00,x
	lda code_2f00,x
	sta $2f00,x
	lda code_3000,x
	sta $3000,x
	lda code_3100,x
	sta $3100,x
	lda code_3200,x
	sta $3200,x
	lda code_3300,x
	sta $3300,x
	lda code_3400,x
	sta $3400,x
;	lda code_3500,x
;	sta $3500,x
;	lda code_3600,x
;	sta $3600,x
;	lda code_3700,x
;	sta $3700,x
;	lda code_3800,x
;	sta $3800,x
;	lda code_3900,x
;	sta $3900,x
;	lda code_3a00,x
;	sta $3a00,x
;	lda code_3b00,x
;	sta $3b00,x
;	lda code_3c00,x
;	sta $3c00,x
;	lda code_3d00,x
;	sta $3d00,x
	dex
	bpl -
        jsr waitvbl

	lda #$0
        sta $3fff
        sta $dd05
	lda #$4c
	sta $dd04
	lda #$40
	sta $dd0c
	lda #<$dd04
	sta $fffa
	lda #>$dd04
	sta $fffb
        lda #%00010000
        sta $dd0e

;!ifdef enable_sprites {
	sei
	lda #$00
	sta $d011
	ldx #100
-	jsr waitvbl
	dex
	bne -
	jsr set_standard_irq_without_colors
	lda #$e5
	sta $d018
	rts
;} else {
!ifdef commented_out {
	jsr enable_openborder_nmi
	ldx #$0f
        lda #$00
-	sta $d000,x        
	clc
        adc #$01
        dex
        bpl -
        lda #$ff
        sta $d015
        
--	ldx #$0f
-	inc $d000,x
	lda $d000,x
        ;ora #$80
        sta $d000,x
	dex
        dex
        bpl -
        jsr waitvbl
	jmp --
}

;code_3d00:
;	nop
;code_3b00:
;	nop
;code_3900:
;	nop
;code_3700:
;	nop
;code_3500:
;	nop
code_3300:
	nop
code_3100:
	nop
code_2f00:
code_2d00:
	jmp $2c01

code_start:
;code_3c00:
;	nop
;code_3a00:
;	nop
;code_3800:
;	nop
;code_3600:
;	nop
code_3400:
	nop
code_3200:
	nop
code_3000:
	nop
code_2e00:
code_2c00:
	nop
code_2b00:
	dec $d016
	inc $d016
	jmp $dd0c
code_max = * - code_start

                
irq_line_f8:
	lda #$00
	sta $d011
	rts

reinit_openirq:
	lda #$f7
	sta $d012
	lda #$00
        !ifdef enable_spritescreen {
	sta irq_counter+1
        }
        !ifdef enable_noise {
	sta irq_counter_noise+1
        }
        !ifdef enable_circle {
	;sta irq_counter_circle+1
        }
	rts


