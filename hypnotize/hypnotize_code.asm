!src "include/memorytab.asm"

!cpu 6510
;!to "shadebobs3.prg",cbm
;--------------------------------------------------
hyp_col1 = $0b	;9
hyp_col2 = $00	;8
hyp_col3 = $00	;a
hyp_col4 = $00	;7

hyp_colors2	=$7f00	;$c000

HYP_SCREEN = $bc00

;--------------------------------------------------

			;*=$0800
hypnotize_init:
			ldx #$00
			txa
-			sta hyp_colors2,x
			inx
			bne -
			
			lda #>sine1
			sta $fb
			ldy #80-1
			lda #$2c*2
			ldx #(-$2c)*2-1
			!ifdef enable_ecmplasma {
			jmp calcsintab
			} else {
			!ifdef enable_openborder {
			jmp calcsintab
			} else {
			jsr calcsintab
			sei
			lda #$35
			sta $01
			}
			lda #$00
			sta writebyte+1
			lda #$b8
			sta writebyte+2
			jmp genfont
			}

hypnotize:
			!ifdef enable_openborder {
                        } else {
                        sei
                        }
			ldx #$00
			stx $d020
			stx $d021
			stx $d022
			stx $d023
			stx $d024
			
			txa
hyp_loop1x
			sta $d800,x
			sta $d900,x
			sta $da00,x
			sta $db00,x
;			sta hyp_colors2,x
			inx
			bne hyp_loop1x
			sta wb+1
			sta change
			lda #>HYP_SCREEN
			sta wb+2


			ldy #$00
lab1
			ldx #$00
lab2
			lda sine1,x
			clc
			adc sine1+7,y
wb			sta HYP_SCREEN,x
			inx
			cpx #40
			bne lab2
			lda wb+1
			clc
			adc #40
			sta wb+1
			lda wb+2
			adc #0
			sta wb+2
			iny
			cpy #25
			bne lab1


			
                        !ifdef enable_sound {
                        } else {
			lda #150
			sta loopcounter	
                        }
hyp_main
			;txa
			;pha
;			lda #$fa
;			cmp $d012
;			bne *-3
			
			;dec $d020
			jsr fadescreen


			ldx change
			txa
			lsr
			lsr
			lsr
			lsr
			lsr
			lsr
			tay
			lda coltab,y
			sta hyp_colors2,x

			inc change
			bne nochange

			lda coltab
			sta spare
			lda coltab+1
			sta spare+1
			lda coltab+2
			sta spare+2
			lda coltab+3
			sta spare+3
			
			ldx #$00
cycle1
			lda coltab+4,x
			sta coltab,x
			inx
			cpx #spare-coltab
			bne cycle1			
nochange

			lda hyp_colors2+$c0
			sta $d021
			lda hyp_colors2
			sta $d022
			lda hyp_colors2+$40
			sta $d023
			lda hyp_colors2+$80
			sta $d024

;			inc $d020
                        !ifdef enable_sound {
                        lda timer
                        bne +
			} else {
			dec loopcounter
			beq +
                        }

			lda #%01011011
			sta $d011
			
			lda #$c8
			sta $d016
			
			lda #%11111110
			sta $d018
			lda #$01
			sta $dd00

			jmp hyp_main

+			
                        !ifdef enable_sound {
                        jsr waitvbl
			jmp set_standard_irq
                        } else {
                        rts
                        }
;--------------------------------------------------
fadescreen
			ldy #$00
ooo1
			isc HYP_SCREEN,y
			ldx HYP_SCREEN,y
			lda hyp_colors2,x
			sta $d800,y

			isc HYP_SCREEN+250,y
			ldx HYP_SCREEN+250,y
			lda hyp_colors2,x
			sta $d800+250,y

			isc HYP_SCREEN+500,y
			ldx HYP_SCREEN+500,y
			lda hyp_colors2,x
			sta $d800+500,y
			
			isc HYP_SCREEN+750,y
			ldx HYP_SCREEN+750,y
			lda hyp_colors2,x
			sta $d800+750,y

			iny
			cpy #$fa
			bne ooo1
			
			rts
;--------------------------------------------------
;writebyte sta $0000
;					inc writebyte+1
;					bne nowb
;					inc writebyte+2
;nowb
;					rts
;--------------------------------------------------
;!src "genfont.asm"
;--------------------------------------------------

