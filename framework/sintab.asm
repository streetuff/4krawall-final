	PI_2 = $e2e5
	ONE = $bfe8
	HALF = $bf11
	PTR_DIV_FAC1 = $bb0f
	LOAD_FAC1 = $bba2 ; a low, y high
	STORE_FAC1 = $bbd4 ;x low, y high
	INT_TO_FAC1 = $b391 ;a low, y high
	FAC1_TO_INT = $bc9b;
	ADD_TO_FAC1 = $b867 ;a low, y high
	MUL_WITH_FAC1 = $ba28 ; a low, y high
	SIN = $e26b
	ADD_HALF_TO_FAC1 = $b849
	multiply_value = $ff
	size_of_table = $fe
	sin_index = $fd
	sin_index_inv =$fc
	dest_pointer = $fa
	dest_pointer2 = $f8
	add_val = $f7
	add_value = $0340
	temp_value = $0360
	mul_value = $0380

	progress_low_byte = $10
	PROGRESS_VAL = $0c

	;in a multiply
	;in y size of table-1
	;in x add value for table
	;in dest_pointer/dest_pointer+1 destination pointer
	;calculates from 0 to 2 * PI
calcsintab:
	stx add_val
	sta multiply_value
	sty size_of_table
	;calculate step
	lda #$00
	sta $68
        sta $fa
	sta sin_index
	jsr INT_TO_FAC1
	lda size_of_table
	sta sin_index_inv
	sec
	adc dest_pointer
	sta dest_pointer2
	lda #$00
	adc dest_pointer+1
	sta dest_pointer2+1
	lsr size_of_table
	bcc +
	inc size_of_table
+	lda #<PI_2
	ldy #>PI_2
	jsr PTR_DIV_FAC1
	ldx #<add_value
	ldy #>add_value
	jsr STORE_FAC1

	ldy multiply_value
	jsr INT_TO_FAC1_BYTE
	ldx #<mul_value
	ldy #>mul_value
	jsr STORE_FAC1
	
	;generate zero for loop
	ldy #$00
	jsr INT_TO_FAC1_BYTE
calcsinloop:
	ldx #<temp_value
	ldy #>temp_value
	jsr STORE_FAC1
	jsr SIN
	lda #<mul_value
	ldy #>mul_value
	jsr MUL_WITH_FAC1
	jsr ADD_HALF_TO_FAC1
	jsr FAC1_TO_INT
	lda $65
	clc
	adc add_val
	ldy sin_index
	inc sin_index
	sta (dest_pointer),y
	sta (dest_pointer2),y
	lda <progress_low_byte
	clc
	adc #PROGRESS_VAL
	sta <progress_low_byte
	bcc +
	
        lda #$a0
.l	sta $0400+11*40+1
	inc .l+1
;	bne +
;	inc .l+2
+
	lda add_val
	sec
	sbc $65
	ldy sin_index_inv
	dec sin_index_inv
	sta (dest_pointer),y
	sta (dest_pointer2),y
	lda #<temp_value
	ldy #>temp_value
	jsr LOAD_FAC1
	lda #<add_value
	ldy #>add_value
	jsr ADD_TO_FAC1
	dec size_of_table
	bpl calcsinloop
	rts

INT_TO_FAC1_BYTE:
	lda #$00
	jmp INT_TO_FAC1


	!ifdef COMMENTED_OUT {
print_fac1:
	ldx #$70
	ldy #$03
	jsr STORE_FAC1
	jsr $bddd
	ldx #$00
-	lda $0100,x
	beq +
	jsr $ffd2
	inx
	bne -
+	lda #$0d
	jsr $ffd2
	lda #$70
	ldy #$03
	jmp LOAD_FAC1
	}
	
	
	


	

