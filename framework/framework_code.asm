!src "include/memorytab.asm"

;----------------------------------------------------
	;macros

	!macro check_same_page_start {
	!set page_check_page_addr = * & $ff00
	}

	!macro check_same_page_end {
	!if (page_check_page_addr != ( * & $ff00)) {
		!error "not the same page"
	}
	}

;----------------------------------------------------
	;in a value
	;in x first page
	;in y number of pages

	;changed : x,y

	!ifdef COMMENTED_OUT {
        
memset:
	stx memset_addr+2
	ldx #$00
memset_addr:
	sta $ff00,x
	inx
	bne memset_addr
	inc memset_addr+2
	dey
	bne memset_addr
	rts
	}
;----------------------------------------------------

	;in a source page
	;in x dest page
	;in y number of pages

	;changed : a,x,y

	!ifdef COMMENTED_OUT {

memcpy:
	sta memcpy_src_addr+2
	stx memcpy_dst_addr+2
	ldx #$00
memcpy_src_addr:
	lda $ff00,x
memcpy_dst_addr:
	sta $ff00,x
	inx
	bne memcpy_src_addr
	inc memcpy_src_addr+2
	inc memcpy_dst_addr+2
	dey
	bne memcpy_src_addr
	rts
	}
;----------------------------------------------------
waitvbl:
-	bit $d011
        bpl -
-	bit $d011
	bmi -
        rts        

;----------------------------------------------------
	!ifdef COMMENTED_OUT {
waitd012:
	;in a line to wait
	;changed: -
	cmp $d012
	bne waitd012
	rts
	}

;----------------------------------------------------

waitstable:
	;stabilize raster position
	;changed: a
	lda #62
	sec
	sbc $dd06
	sta waitstablebpl+1
waitstablebpl:
	bpl *
	!for i, 60 {
	!byte $c9
	}
	bit $ea
	rts


;----------------------------------------------------

init_stable_timer:
	; make a stable timer on CIA A
	; this is needed for stable interrupts
	; must be done only once at start

	;changed : a,x,y

	; no interrupts please in this function !
	; reset timer
	ldx #$00
        ;lda #$7f
        ;sta $dd0d,x
	;sta $dc0d,x
        lda #%00000000
        sta $dd0f
	; 63 to zero because of 63 cycles per line
        lda #<(63-1)
        sta $dd06
        lda #>(63-1)
        sta $dd07
	; but not start it yet !

	+check_same_page_start
	; get out of bad line area
-	lda $d011
	bmi -
-	lda $d011
	bpl -
	lda $d012
-	cmp $d012
	beq -
	lda (00,x)
        ldy #%00010001
again:
	;make a loop with 62 cycles
	ldx #10   ; 2
-	dex	  ; 2
	bne -     ; 3 (last one 2) -> 2+(10*5)-1
	lda $d012 ; 4
	;if the compare is not equal the first time, we are in sync with VIC !
-	cmp $d012 ; 4
	beq again ; 3 -> 4+4+3 + (2+(10*5)-1) = 62 (63-1 !)
	; start the timer now
	lda (00,x)
	lda (00,x)
	lda (00),y
	nop
        sty $dd0f
	+check_same_page_end
	rts

standard_irq:
	pha
	txa
	pha
	tya
	pha
	inc $d019
        !ifdef enable_sound {
	jsr sound_main
        }
end_irq:
        pla
        tay
        pla
        tax
        pla
        rti

;----------------------------------------------------
	;changed : a,x,y
wait_end:
	!ifdef enable_sound {
-       lda timer
	beq -
        } else {
	ldx #150
-
	jsr waitvbl
	dex
	bne -
	}
;----------------------------------------------------

set_standard_irq_without_colors:
	!ifdef enable_openborder {
	jsr disable_openborder_nmi
        } else {
        }
        jsr change_color
	jmp set_standard_irq_without_colors_cont

	;changed : a,x,y
set_standard_irq:
	!ifdef enable_openborder {
	jsr disable_openborder_nmi
        } else {
        }
        jsr change_color
	lda colors
	sta $d020
	sta $d021
set_standard_irq_without_colors_cont:
        lda #$00
        sta $d015
        !ifdef enable_sound {
        sta timer
        }
	lda #$1b
	sta $d011
	lda #$e5
	sta $d018
	lda #$03
	sta $dd00
        lda #$ff
        sta $d012
	inc $d019
	ldx #<standard_irq
	ldy #>standard_irq
;----------------------------------------------------

	;changed : -
set_irq:
	sei
	stx $fffe
	sty $ffff
	cli
	rts

colorpos = $02        
 
change_color:
	ldx #$00
        ldy #$00
-	lda color_table,x
	sta colors,y
        iny
        inx
        cpy #$04
        bne -
        lax change_color+1
        sbx #$fc
        cpx #colortable_len
        bcc +
        ldx #$00   
+	stx change_color+1
        rts
 
