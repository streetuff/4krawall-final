!src "include/memorytab.asm"

sinbar_codegen_counter_7 = $07
sinbar_codegen_counter_21 = $08
sinbar_codegen_counter_total = $09
zp_02 = $02
zp_04 = $04
zp_irq = $06

precalc_sinbar:
	;generate sintabs
	lda #>sinbar_sintab1
	sta $fb
	ldy #$7f
	lda #11
	ldx #12
	jsr calcsintab

	lda #>sinbar_sintab2
	sta $fb
	ldy #$3f
	lda #11
	ldx #12
	jsr calcsintab

	lda #>sinbar_sintab3
	sta $fb
	ldy #$7f
	lda #11
	ldx #12
	jsr calcsintab

        ldx #$00
-	lda sinbar_sintab2,x
	sta sinbar_sintab2+$80,x
        inx
        bpl -       
        
	;generate sprites
	lda $01
	pha
	lda #$30
	sta $01
	ldx #<sinbar_sprites
	ldy #>sinbar_sprites
	jsr set_codegen_start
	lda #12
	sta sinbar_codegen_counter_7

copy_single_sprite:
	lda #21
	sta sinbar_codegen_counter_21
-	lda #<sinbar_spriteline
	ldx #>sinbar_spriteline
	ldy #$03
	jsr copy_code_fragment
	dec sinbar_codegen_counter_21
	bne -
	inc codegen_ptr
	bne +
	inc codegen_ptr+1
+
	asl sinbar_spriteline+2
	rol sinbar_spriteline+1
	rol sinbar_spriteline
	bcc +
	inc sinbar_spriteline+2
+	dec sinbar_codegen_counter_7
	bne copy_single_sprite

	;generate table
	ldx #<$c7f8
	ldy #>$c7f8
	jsr set_codegen_start
	ldx #(sinbar_sprites&$3fff)/64
--	txa
	ldy #$07
-	sta (codegen_ptr),y
	dey
	bpl -
	lda codegen_ptr+1
	clc
	adc #$04
	sta codegen_ptr+1 
	inx
	cpx #12+(sinbar_sprites&$3fff)/64
	bne --

	pla
	sta $01

	ldy #$00
--	ldx #$10
-	txa
	sbx #$100-$10
	sta sinbar_d018_table,y
	sta sinbar_d018_table+$100-4,y
	cmp #$d0
	beq --
	iny
	bne -
        
        ;genrate addtab
        tya
-
	clc
        adc #$01
        ora #$80
	sta sinbat_addtab,y
        iny
	bne -             
        
	;generate code
	
	ldx #<sinbar_codegenadr
	ldy #>sinbar_codegenadr
	jsr set_codegen_start
	lda #$09
	sta sinbar_codegen_counter_total
	lda #$06
	sta sinbar_codegen_counter_7
precalc_sinbar_loop:
	lda #42
	sta sinbar_codegen_counter_21
	lda #$01
	sta sinbar_codesnippet_addr
precalc_sinbar_loop_small:
	lda #<sinbar_codesnippet
	ldx #>sinbar_codesnippet
	ldy #sinbar_codesnippet_len
	jsr copy_code_fragment
	lda sinbar_codegen_counter_total
        beq +
	cmp #$f8
	bne ++
+	lda #$05
        jsr sub_code_offset
	lda #<sinbar_codesnippet_f9
	ldx #>sinbar_codesnippet_f9
	ldy #sinbar_codesnippet_f9_len
	jsr copy_code_fragment
	lda #$08
	sta sinbar_codesnippet_f9_data
	bne ++

++

	lda sinbar_codesnippet_addr
	cmp #$0f
	beq ++
	adc #$02
	sta sinbar_codesnippet_addr
++
;	lda sinbar_codesnippet_test
;	eor #$02
;	sta sinbar_codesnippet_test
	inc sinbar_codegen_counter_total
	dec sinbar_codegen_counter_21
	bne precalc_sinbar_loop_small
	lax sinbar_codesnippet_data
	sbx #$100-42
	stx sinbar_codesnippet_data
	dec sinbar_codegen_counter_7
        bne +
        lda #40
        sta precalc_sinbar_loop+1
        lda #$06
        sta sinbar_codesnippet_data
+	bpl precalc_sinbar_loop
	;lda #sinbar_codesnippet_len*2
	;jsr sub_code_offset	
	jmp end_code_with_rts


sinbar_codesnippet_f9:
sinbar_codesnippet_f9_data = *+1
        lda #$00   ; 2
        sta $d011  ; 4         
sinbar_codesnippet_f9_len = * - sinbar_codesnippet_f9

sinbar_codesnippet:
        dec $d016
        inc $d016
        lda (zp_02),y ; 6
        adc (zp_04),y ; 6
        tax        ; 2
        lda sinbat_addtab,y
        tay
        lda sinbar_d018_table,x ; 4
        sta $d018   ; 4
sinbar_codesnippet_data = *+1
        lda #$06+42   ; 2
sinbar_codesnippet_addr = *+1
        sta $d001  ; 4         
sinbar_codesnippet_len = * - sinbar_codesnippet

        
        
sinbar:
	lda #$f9
        sta $d012
	ldx #<disable_badline_irq
        ldy #>disable_badline_irq
        jsr set_irq
	lda #$4c
        sta zp_irq
        lda #<sinbar_irq
        sta zp_irq+1
        lda #>sinbar_irq
        sta zp_irq+2
	lda #$80
        sta zp_02
        sta zp_04
        lda #(>sinbar_sintab1)-1
        sta zp_02+1
sinbar_next_frame:
        lda #(>sinbar_sintab3)-1
        sta zp_04+1
	lda #(>sinbar_sintab2)-1
	sta sinbar_next_frame+1
	jsr set_sprites_for_fullscreen
        jsr waitvbl
        ;sei
        lda #$06
        sta $d012
        lda #$08
        sta $d011
        sta $dd00
	ldx #<zp_irq
        ldy #>zp_irq
        jsr set_irq
	lda #$ff
        sta $d017
	jmp wait_end

sinbar_irq:
	pha
        txa
        pha
        tya
        pha
sinbar_irq_addtab:        
        ldy #$80
        clc
        inc $d019
        jsr waitstable
        lda (00,x)
        lda (00,x)
        lda (00,x)
        lda (00,x)
        jsr sinbar_codegenadr
        lax zp_02
        sbx #$02
        txa
        ora #$80
        sta zp_02
        lax zp_04
        sbx #$fe
        txa
        ora #$80
        sta zp_04
        ldx sinbar_irq_addtab+1
        inx
        txa
        ora #$80
        sta sinbar_irq_addtab+1
        !ifdef enable_sound {
	jsr sound_main
        }
        jmp end_irq
