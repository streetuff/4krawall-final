SHELL = /bin/bash

DIRS = framework sprites sound openborder spritescreen codegen circle sinbar genfont ecmplasma \
	hypnotize stretch kefrens d020

all:
	DIRS="$(DIRS)" make 4krawall

include Makefiles/rules

test:
	@for i in $(DIRS); do make -C $$i; done;

