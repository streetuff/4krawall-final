
newlinetab:
	;!byte $ff
	!byte $1c
	!byte $06
	!byte LINE_SPLIT1
linetab:
	!byte LINE_SPLIT2
	!byte LINE_SPLIT3
	!byte LINE_SPLIT4
	!byte LINE_SPLIT5-1
	!byte LINE_SPLIT6-2
	!byte LINE_SPLIT7-3
	!byte $f7

MAX_IRQS = * - newlinetab

irq_low_tab:
	!byte <irq_line_f8_2
	;!byte <irq_line_ff
	!byte <irq_split_08
	!byte <irq_split_00
	!byte <irq_split
	!byte <irq_split
	!byte <irq_split
	!byte <irq_split
	!byte <irq_split
	!byte <irq_split
	!byte <irq_split_07

irq_high_tab:
	!byte >irq_line_f8_2
	;!byte >irq_line_ff
	!byte >irq_split_08
	!byte >irq_split_00
	!byte >irq_split
	!byte >irq_split
	!byte >irq_split
	!byte >irq_split
	!byte >irq_split
	!byte >irq_split
	!byte >irq_split_07


