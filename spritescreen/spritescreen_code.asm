
!src "include/memorytab.asm"

SS_BASE_ZP = $02

SPACE = 35
LINE_SPLIT1 = $0f
LINE_SPLIT2 = $0f+SPACE
LINE_SPLIT3 = $0f+SPACE*2
LINE_SPLIT4 = $0f+SPACE*3
LINE_SPLIT5 = $0f+SPACE*4
LINE_SPLIT6 = $0f+SPACE*5+1
LINE_SPLIT7 = $0f+SPACE*6+1

disable_badline_irq_spritescreen:
	pha
	txa
	pha
	tya
	pha
irq_counter:
	ldy #$00
	lax newlinetab,y
	dex
	stx $d012
	inc $d019
	lda irq_low_tab,y
	sta irq_jump+1
	lda irq_high_tab,y
	sta irq_jump+2
	iny
	cpy #MAX_IRQS
	bne +
	ldy #$00
+	sty irq_counter+1

irq_jump:
	jsr $0
	jmp end_irq


irq_split_07:
	lda #<LINE_SPLIT7+SPACE
	bne do_linesplit


irq_line_f8_2:
	lda #$80
	sta $d011
        lda #$15
        sta $d018
        lda #$03
        sta $dd00
	rts

irq_split_00 = enable_openborder_nmi

irq_split_08:
	lda #$08
	sta $d011
	jsr disable_openborder_nmi
        !ifdef enable_sound {
	jsr sound_main
        }
	;sta $d021
base_jump_base:
	lda #$00
	sta base_jump+1
	lax base_jump_base+1 
	sbx #$04
	stx base_jump_base+1
	lda #LINE_SPLIT1
	bne do_linesplit

do_linesplit_add = $ee

irq_split:
	lda linetab-4,y
	;sec
	;sbc #$03
do_linesplit:
	cli
	sta do_linesplit_add
base_jump:
	ldx #$00
	jsr generated_code
	lax base_jump+1
new_base_jump = *+1        
	sbx #$100-$01	
	stx base_jump+1
	rts


precalc_spritescreen:
	lda #>sintab1
	sta $fb
	ldy #$7f
	lda #05
	ldx #$00
	jsr calcsintab
	lda #>sintab2
	sta $fb
	ldy #$7f
	lda #09
	ldx #$fc
        jsr calcsintab
	ldx #<generated_code
	ldy #>generated_code
	jsr set_codegen_start

-	
d000tab_ptr:
	lda d000tab
	sta snippet1_tab_add
	lda #<snippet1
	ldx #>snippet1
	ldy #snippet1_len
	jsr copy_code_fragment
	inc d000tab_ptr+1
;        bne +
;	inc d000tab_ptr+2
;+        
	inc snippet1_zp_addr
	lda snippet1_zp_addr
	cmp #$0a
	bne -

	lda #$03
	jsr sub_code_offset

	lda #<snippet1_1
	ldx #>snippet1_1
	ldy #snippet1_1_len
	jsr copy_code_fragment

-	lda #<snippet2
	ldx #>snippet2
	ldy #snippet2_len
	jsr copy_code_fragment
	inc snippet2_addr
	inc snippet2_addr
	lda snippet2_addr
	cmp #$11
	bne -

	lda #$03
	jsr sub_code_offset

	lda #<snippet4
	ldx #>snippet4
	ldy #snippet4_len
	jsr copy_code_fragment

-	lda #<snippet3
	ldx #>snippet3
	ldy #snippet3_len
	jsr copy_code_fragment
	inc snippet3_addr
	inc snippet3_addr
	inc snippet3_zp_addr
	lda snippet3_zp_addr
	cmp #$0a
	bne -
	jmp end_code_with_rts

snippet1:
	lda sintab2,x
snippet1_tab_add = *+1
	adc #$00
snippet1_zp_addr = *+1
	sta SS_BASE_ZP
	txa
	sbx #$f1
snippet1_len = *-snippet1

snippet1_1:
	txa
	asl
	tax
snippet1_1_len = *-snippet1_1

snippet2:
	lda sintab1,x
	adc do_linesplit_add
snippet2_addr = *+1
	sta $d001
	txa
	sbx #$f7
snippet2_len = * - snippet2

snippet3:
snippet3_zp_addr = *+1
	lda SS_BASE_ZP
snippet3_addr = *+1
	sta $d000
snippet3_len = *-snippet3


snippet4:
	jsr rts_addr
snippet4_len = *-snippet4
	
spritescreen:
	jsr setup_spritesblocks_4krawall
        lax new_base_jump
        sbx #$04
        stx new_base_jump
	;jsr waitvbl
	lda #$ff
	sta $d015
	sta $d01d
	lda #$00
        sta $d017
	sta $d01c
	sta $3fff
	jsr reinit_openirq
	ldx #<disable_badline_irq_spritescreen
	ldy #>disable_badline_irq_spritescreen
	jsr set_irq
	lda #$c1
	sta $d010
	jmp wait_end




