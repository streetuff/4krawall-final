!src "include/memorytab.asm"

KEF_FIRSTLINE = $1a
KEF_NUMBER_OF_LINES = $6a
KEF_ZP_SCROLL = $02

KEF_SPRITEBLOCK = $0100

kefrens_stack_value = $fe

kefrens:
	lda #$00
        sta $3fff
	lda #$f9
        sta $d012
	ldx #<disable_badline_irq
        ldy #>disable_badline_irq
        jsr set_irq
	jsr kef_calc
	jsr set_sprites_for_fullscreen
	lda colors+1
	sta $d025
	lda colors
	sta $d020
	sta $d021
	lda colors+3
	sta $d026
	ldx #$07
-	lda #KEF_SPRITEBLOCK/$40	
	sta $07f8,x
	lda #KEF_SPRITEBLOCK/$40+1
	sta $03f8,x	
	dex
	bpl -
	ldy #$0e
	lda #KEF_FIRSTLINE-7
-	sta $d001,y
	adc #$01
	dey
	dey
	bpl -
	tsx
	dex
	dex
	stx kefrens_stack_value
        jsr waitvbl
	lda #$05
        sta d018_val+1
	sta $d018
kefrens_loop:
	ldx #$3e
	lda #$00
-	sta KEF_SPRITEBLOCK,x
	dex
	bpl -
d018_val:
	lda #$05
	sta $d018
	stx $d015
	lda #KEF_FIRSTLINE
-	cmp $d012
	bne -
	stx $d01c
	ldy #KEF_NUMBER_OF_LINES
	jsr waitstable
	!for i, 10 {
	nop
	}
	jsr codegen_kefrens
	jsr kef_calc
	lda #$15
	sta d018_val+1
	!ifdef enable_sprites {
	lda timer
	beq +
	lda #$2c
	sta kef_dec_addr
	;lda #$ee
	;sta kef_inc_addr
	jmp set_standard_irq_without_colors
+
	}
	jmp kefrens_loop

kef_calc:
	;dec $d020
	ldx #KEF_NUMBER_OF_LINES-1
	clc
kef_calc_loop:
kef_sintab1_ptr = *+1
	lda kefrens_sintab,x
kef_sintab2_ptr = *+1
        adc kefrens_sintab2,x
	sta KEF_ZP_SCROLL,x
	dex	
	bpl kef_calc_loop
	dec kef_sintab1_ptr
kef_dec_addr:
	dec kef_sintab1_ptr
kef_inc_addr:
	inc kef_sintab2_ptr
	inc kef_sintab2_ptr
	inc kef_sintab2_ptr
	;inc $d020
	rts

kefrens_snippet:
	stx $d017
	dec $d016
	inc $d016
	inc $d017
kefrens_snippet_zp_scroll = *+1
	ldy KEF_ZP_SCROLL
        ldx kefrens_addresses,y
	txs
	lda $0100,x
	and kefrens_last_ands,y
	ora kefrens_last_ors,y
	dec $d017
	dec $d016
	inc $d016
	inc $d017

	pha
	lda $00ff,x
	and kefrens_first_ands,y
	ora kefrens_first_ors,y
	pha
	ldx #$ff
        bit $ea        
kefrens_snippet_len = * - kefrens_snippet

kefrens_end_snippet:
	lda #$05
	sta $d018
	lda #$00
	sta $d015
	ldx kefrens_stack_value
	txs
	rts
kefrens_end_snippet_len = * - kefrens_end_snippet


kefrens_init:
	lda #>kefrens_sintab
        sta $fb
        ldy #$ff
        lda #34
        ldx #50/2
        jsr calcsintab
	lda #>kefrens_sintab2
        sta $fb
        ldy #$7f
        lda #16
        ldx #50/2
        jsr calcsintab
	ldx #$3f
        lda #$00
-	sta KEF_SPRITEBLOCK+$40-1,x
	dex
	bne -
-	lda kefrens_sintab2,x
	sta kefrens_sintab2+$100,x
	inx
	bne -
-	txa
	lsr
        lsr
	anc #$1f
	adc #<KEF_SPRITEBLOCK+6
	sta kefrens_addresses,x
	txa
	and #$03
	tay
	lda kef_mask_col0,y
	sta kefrens_first_ands,x
	lda kef_dat_col0,y
	sta kefrens_first_ors,x
	lda kef_mask_col1,y
	sta kefrens_last_ands,x
	lda kef_dat_col1,y
	sta kefrens_last_ors,x
	inx
	bne -
	ldx #<codegen_kefrens
	ldy #>codegen_kefrens
	jsr set_codegen_start
kefrens_init_loop:
	lda #<kefrens_snippet
	ldx #>kefrens_snippet
	ldy #kefrens_snippet_len
	jsr copy_code_fragment
	inc kefrens_snippet_zp_scroll
	lda kefrens_snippet_zp_scroll
	cmp #KEF_ZP_SCROLL+KEF_NUMBER_OF_LINES
	bne kefrens_init_loop
	lda #<kefrens_end_snippet
	ldx #>kefrens_end_snippet
	ldy #kefrens_end_snippet_len
	jsr copy_code_fragment
	rts

