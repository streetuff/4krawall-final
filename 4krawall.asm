;-------------------------------------------------------
;
; 4krawall main file
;
;-------------------------------------------------------

timer		= $f9

	!cpu 6510
        !to "4krawall_plain.prg",cbm
        
        !src "config.asm"
        * = $0800
krawall_sprites:
;-------------------------------------------------------
;
; gfx for the parts
;
;-------------------------------------------------------
	!bin "sprites/sprites.raw"

;-------------------------------------------------------
;
; entry point. If this sould be changed, change it in Makefiles/rules
;
;-------------------------------------------------------
        * = $0a40
;-	lda $dc01
;	and #$10
;       bne -
	jsr $ff81
        jsr $ff90
        sei
	lda #$7f
	sta <progress_low_byte
        sta $dc0d
	lda $dc0d
	ldx #38
	lda #$40
-	sta $0400+10*40,x
	sta $0400+12*40,x
	dex
	bne -
	stx $d020
	stx $d021
-	lda #$20
	sta $3800,x
	sta $3900,x
	sta $3a00,x
	sta $3af8,x
	lda #$01
	sta $d900,x
	sta $da00,x
        lda #$0140/64
        sta $3bf8,x
	inx
	bne -
partloop:
	txa
	pha
	lda prepare_functions,x
        sta prepare_jump+1
        lda prepare_functions+1,x
        beq end_prepare
        sta prepare_jump+2
prepare_jump:
	jsr $0
        pla
        tax
        inx
        inx
        bne partloop

end_prepare:
	pla
        ;cli
        ldx #part_functions-prepare_functions
        bne partloop ;unconditional

;-------------------------------------------------------
;
; code sources of the parts
;
;-------------------------------------------------------
                       
        !ifdef enable_framework {
        !src "framework/sintab.asm"
        !src "framework/framework_code.asm"
        }
	!ifdef enable_codegen {
        !src "codegen/codegen_code.asm"
        }
	!ifdef enable_genfont {
        !src "genfont/genfont_code.asm"
        }
	!ifdef enable_sprites {
        !src "sprites/sprites_code.asm"
        }
        !ifdef enable_openborder {
        !src "openborder/openborder_code.asm"
        }
	!ifdef enable_noise {
        !src "noise/noise_code.asm"
        }
	!ifdef enable_sinbar {
        !src "sinbar/sinbar_code.asm"
        }
	!ifdef enable_spritescreen {
        !src "spritescreen/spritescreen_code.asm"
        }
	!ifdef enable_circle {
        !src "circle/circle_code.asm"
        }
	!ifdef enable_ecmplasma {
        !src "ecmplasma/ecmplasma_code.asm"
        }
	!ifdef enable_hypnotize {
        !src "hypnotize/hypnotize_code.asm"
        }
	!ifdef enable_sound {
        !src "sound/sound_code.asm"
        }
	!ifdef enable_d020 {
	!src "d020/d020_code.asm"
	}
        !ifdef enable_stretch {
        !src "stretch/stretch_code.asm"
        }
	!ifdef enable_kefrens {
	!src "kefrens/kefrens_code.asm"
	}
	!ifdef enable_endpic {
	!src "endpic/endpic_code.asm"
	}


;-------------------------------------------------------
;
; prepare function of the parts
; are called once before the demo begins
; caution: before "openborder_prepare" $01 is $37, after that $35
;
;-------------------------------------------------------

prepare_functions:
	!ifdef enable_kefrens {
	!word kefrens_init
	}
	!ifdef enable_sinbar {
        !word precalc_sinbar
	}
	!ifdef enable_spritescreen {
        !word precalc_spritescreen
	}
	!ifdef enable_circle {
        !word precalc_circle
	}
	!ifdef enable_noise {
        !word precalc_noise
	}
        !ifdef enable_ecmplasma {
	!word ecmplasma_init
	}
	!ifdef enable_hypnotize {
	!word hypnotize_init
	}
        !ifdef enable_stretch {
        !word stretch_init
        }
	!ifdef enable_sound {
	!word sound_init
	}
        !ifdef enable_openborder {
        	;before "openborder_prepare", $01 has value $37
	!word openborder_prepare
        	;now $01 has the value $35
        } else {
	!ifdef enable_framework {
	!word set_standard_irq
	}
        }
        !word 0

;-------------------------------------------------------
;
; part functions that are called in the demo
;
;-------------------------------------------------------
        
part_functions:
        !ifdef enable_hypnotize { 
        !word hypnotize
        }
	!ifdef enable_noise {
        !word noise
	}
        !ifdef enable_spritescreen { 
        !word spritescreen
        }
        !ifdef enable_sinbar { 
        !word sinbar
        }
        !ifdef enable_circle { 
        !word circle
        }
        !ifdef enable_ecmplasma { 
        !word ecmplasma
        }
        !ifdef enable_stretch { 
        !word stretch
        }
	!ifdef enable_d020 {
	!word d020
	}
	!ifdef enable_kefrens {
	!word kefrens
	} else {
	!ifdef enable_endpic {
	!word endpic
	}
	}
        !word 0

endpic_ptr:
	!ifdef enable_endpic {
	!word endpic
	}
        
;-------------------------------------------------------
;
; data sources of the parts
;
;-------------------------------------------------------
	* = (* + $ff) & $ff00

	!ifdef enable_sound {
        !src "sound/sound_data.asm"
        }
	!ifdef enable_sprites {
        !src "sprites/sprites_data.asm"
        }
	!ifdef enable_ecmplasma {
        !src "ecmplasma/ecmplasma_data.asm"
        }
	!ifdef enable_hypnotize {
        !src "hypnotize/hypnotize_data.asm"
        }
	!ifdef enable_spritescreen {
        !src "spritescreen/spritescreen_data.asm"
        }
 	!ifdef enable_noise {
        !src "noise/noise_data.asm"
        }
	!ifdef enable_sinbar {
        !src "sinbar/sinbar_data.asm"
        }
        !ifdef enable_stretch {
        !src "stretch/stretch_data.asm"
        }
	!ifdef enable_genfont {
        !src "genfont/genfont_data.asm"
        }
        !ifdef enable_framework {
        !src "framework/framework_data.asm"
        }
        !ifdef enable_kefrens {
        !src "kefrens/kefrens_data.asm"
        }
        !ifdef enable_endpic {
        !src "endpic/endpic_data.asm"
        }
