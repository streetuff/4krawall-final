!src "include/memorytab.asm"

C_SPR0 = $08
C_SPR1 = $0a
C_SPR2 = $0c
C_SPR3 = $0e
C_SPR4 = $00
C_SPR5 = $02
C_SPR6 = $04
C_SPR7 = $06


circle_first = $ff

!macro irq_circle_first {
        lda circle_first
        ora #$b0
        tax
        ldy #C_SPR7
        lda circle_spr_tab
        jsr paintcircle
        txa
        sbx #$10
        
        ldy #C_SPR6
        lda circle_spr_tab+1
        jsr paintcircle
        txa
        sbx #$10

        ldy #C_SPR5
        lda circle_spr_tab+2
        jsr paintcircle
        txa
        sbx #$10

        ldy #C_SPR4
        lda circle_spr_tab+3
        jsr paintcircle
        txa
        sbx #$10

        ldy #C_SPR3
        lda circle_spr_tab+4
        jsr paintcircle
        txa
        sbx #$10

        ldy #C_SPR2
        lda circle_spr_tab+5
        jsr paintcircle
        txa
        sbx #$10

        ldy #C_SPR1
        lda circle_spr_tab+6
        jsr paintcircle
        txa
        sbx #$10

        ldy #C_SPR0
        lda circle_spr_tab+7
        jsr paintcircle

        jsr enable_openborder_nmi
        }

!macro irq_circle_mid1 {
        lda circle_first
        ora #$30
        tax
        ldy #C_SPR2
        lda circle_spr_tab
        jsr paintcircle
        txa
        sbx #$10
        ldy #C_SPR3
        lda circle_spr_tab+1
        jsr paintcircle
        txa
        sbx #$100-$a0
        ldy #C_SPR6
        lda circle_spr_tab+7
        jsr paintcircle
        txa
        sbx #$100-$10
        ldy #C_SPR5
        lda circle_spr_tab+6
        jsr paintcircle
        }

!macro irq_circle_mid2 {
        lda circle_first
        ora #$10
        tax
        ldy #C_SPR0
        lda circle_spr_tab+2
        jsr paintcircle
        txa
        sbx #$10
        ldy #C_SPR1
        lda circle_spr_tab+3
        jsr paintcircle
        txa
        sbx #$10
        ldy #C_SPR4
        lda circle_spr_tab+4
        jsr paintcircle
        txa
        sbx #$10
        ldy #C_SPR7
        lda circle_spr_tab+5
        jsr paintcircle
        }
        
!macro irq_circle_end {
        jsr disable_openborder_nmi
        jsr set_circle
        }
        
set_circle:

circle_x_pos = *+1
        lda #$00
        pha
        bpl +
        asl
        eor #$ff
        adc #$00
        ldy #$80
        bmi ++

+       asl
        ldy #$00
++      sta paintcircle_x_diff
        sty paintcircle_x_diff_high
        
        pla
        clc
circle_x_pos_diff = *+1
        adc #$01
        sta circle_x_pos
        cmp #($b8)/2
        beq +
        cmp #$ec
        bne ++
+       sec
        sbc circle_x_pos_diff
        sta circle_x_pos
        sec
        lda #$00
        sbc circle_x_pos_diff
        sta circle_x_pos_diff
++      
circle_y_pos = *+1
        ldx #$00
        lda circle_y_movetab,x
        sta paintcircle_y_diff
+       clc
        adc #$48
        sta newlinetab_circle_mid1_line
        adc #$77-$47
        sta newlinetab_circle_mid2_line
        lax circle_y_pos
circle_y_pos_faster = *+1        
        sbx #$fc
        stx circle_y_pos
        
        lda circle_first
        clc
        adc #$01
        and #$0f
        sta circle_first
        bne +
        lda circle_spr_tab
        pha
        ldx #$00
-       lda circle_spr_tab+1,x
        sta circle_spr_tab,x
        inx
        cpx #$07
        bne -
        pla
        sta circle_spr_tab+7
+       rts

               


paintcircle:
        pha
        clc
paintcircle_x_diff_high = *+1
        lda #$00
        bpl paintcircle_posi
        lda circletab,x
        sbc paintcircle_x_diff 
        bcs ++
        lda #$00
        beq ++

paintcircle_posi:
paintcircle_x_diff = *+1
        lda #$00
        adc circletab,x
        bcc ++
        cmp #$5f
        bcc +
        lda #$5f
+       sta $d000,y
        lda $d010
        ora d010ortab,y
        sta $d010
        bne ++++
++      sta $d000,y
        lda $d010
        and d010andtab,y
        sta $d010

++++
        lda circletab+0x40,x
        clc
paintcircle_y_diff = *+1
        adc #$00
        bcc +
        lda #$ff
+       sta $d001,y
        tya
        lsr
        tay
        pla
        sta $07f8,y
        rts

precalc_circle:
        ldx #$00
        stx $fa
        lda #$61
        sta $fb
        lda #$80
        sta $ff
        ldy #$fe
-       lda $fa
        ;clc
        adc $ff
        sta $fa
        tya
        adc $fb
        sta circle_y_movetab,x
        sta $fb
        lda $ff
        clc
        adc #$03
        sta $ff
        bcc +
        iny
+       inx
        bne -        
                        
        jsr set_circle
        lda #>circletab
        sta $fb
        ldy #$ff
        lda #$5e
        ldx #$68
        jmp calcsintab


circle:
        jsr setup_spritesblocks_4krawall
        !ifdef enable_sound {
        } else {
        lda #$06
        sta $d021
        }
        lda #$ff
        sta $d015
        lda #$f8
        sta $d012
        ldx #<disable_badline_irq
        ldy #>disable_badline_irq
        jsr set_irq
        lda #$00
        sta $d01c
        sta $d01d
        sta $d017
        sta $3fff
-       jsr waitvbl
circleloop:        
        lda #$03
-       cmp $d012
        bne -
        +irq_circle_first

newlinetab_circle_mid1_line = *+1
        lda #$00
-       cmp $d012
        bcs -
        +irq_circle_mid1

newlinetab_circle_mid2_line = *+1
        lda #$00
-       cmp $d012
        bcs -
        +irq_circle_mid2

-       lda $d011
        bpl -
        lda #$20
-       cmp $d012
        bcs -
        +irq_circle_end        
                
        !ifdef enable_sound {
        lda timer
        bne +
        }
        lda #$15
        sta $d018
        jmp circleloop
+       lda #$fa
	sta circle_y_pos_faster
	jmp set_standard_irq
